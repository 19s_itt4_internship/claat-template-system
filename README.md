# Claat system

Tool for generating static pages

There is a guide called **"How to use claat"**  located here: [ITT Guides](https://19s_itt4_internship.gitlab.io)

The pages generated for this repo is on [Claat Guide pages](https://19s_itt4_internship.gitlab.io/claat-template-system/)
There is an additional guide on the second page, unlisted on the other page as its only for testing purpose. It can be accessed directly from this page.

## Goal

The main goal is to create a static generator for exercises for next semester in ITT

## Description

The program takes an input in form of a resource location, which can either be a Google Doc ID, markdown, local file path or an arbitrary URL.
It then converts the input into a codelab format, HTML by default.